<?php
    include("includes/head.php");
?>


<section class="error">
    <?php
        include("includes/header.php");
    ?>
    <div class="error_landing">
      <div class="error_landing_inner">
        <p class="404_text">404</p>
        <div class="not_found_box">
          <p>not found!</p>
          <a href="index.php"><img src="img/error_arrow.svg" alt="">əsas səhifəyə dön</a>
        </div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>


<?php
    include("includes/script.php");
?>