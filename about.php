<?php
    include("includes/head.php");
?>


<section class="about">
    <?php
        include("includes/header.php");
    ?>
    <div class="swiper-container">
      <div class="swiper-wrapper">
        <div class="swiper-slide">
            <img src="img/about_back.jpg"  alt="">
            <div class="absolute_background_slider">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <p class="content_title">2010-cu ildən bəri sizinlə</p>
                            <p class="content_info">
                                Polemak Group MMC 2010- cu ildən bəri ölkəmizin sənayesinə o cümlədən inkişafına müəyyən dəstək və məqsədi ilə yaradılmış,
                                plastik məhsulların istehsalı və satışı ilə məşğul olan ticarət şirkətidir.  Mütəxəssislərimiz plastik məhsullar ilə nece davranmalı
                                olduğunu bilir və bununla plastik dünyasında yeni bir dövr açır.
                            </p>
                        </div>
                    </div>
                </div>
                <button><img src="img/arrow_about.svg" alt=""><span>Ətraflı</span></button>
            </div>
        </div>
        <div class="swiper-slide">
            <img src="img/about_back.jpg"  alt="">
            <div class="absolute_background_slider">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <p class="content_title">2010-cu ildən bəri sizinlə</p>
                            <p class="content_info">
                                Polemak Group MMC 2010- cu ildən bəri ölkəmizin sənayesinə o cümlədən inkişafına müəyyən dəstək və məqsədi ilə yaradılmış,
                                plastik məhsulların istehsalı və satışı ilə məşğul olan ticarət şirkətidir.  Mütəxəssislərimiz plastik məhsullar ilə nece davranmalı
                                olduğunu bilir və bununla plastik dünyasında yeni bir dövr açır.
                            </p>
                        </div>
                    </div>
                </div>
                <button><img src="img/arrow_about.svg" alt=""><span>Ətraflı</span></button>
            </div>
        </div>
        <div class="swiper-slide">
            <img src="img/about_back.jpg"  alt="">
            <div class="absolute_background_slider">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <p class="content_title">2010-cu ildən bəri sizinlə</p>
                            <p class="content_info">
                                Polemak Group MMC 2010- cu ildən bəri ölkəmizin sənayesinə o cümlədən inkişafına müəyyən dəstək və məqsədi ilə yaradılmış,
                                plastik məhsulların istehsalı və satışı ilə məşğul olan ticarət şirkətidir.  Mütəxəssislərimiz plastik məhsullar ilə nece davranmalı
                                olduğunu bilir və bununla plastik dünyasında yeni bir dövr açır.
                            </p>
                        </div>
                    </div>
                </div>
                <button><img src="img/arrow_about.svg" alt=""><span>Ətraflı</span></button>
            </div>
        </div>
        <div class="swiper-slide">
            <img src="img/about_back.jpg"  alt="">
            <div class="absolute_background_slider">
                <div class="content">
                    <div class="container">
                        <div class="row">
                            <p class="content_title">2010-cu ildən bəri sizinlə</p>
                            <p class="content_info">
                                Polemak Group MMC 2010- cu ildən bəri ölkəmizin sənayesinə o cümlədən inkişafına müəyyən dəstək və məqsədi ilə yaradılmış,
                                plastik məhsulların istehsalı və satışı ilə məşğul olan ticarət şirkətidir.  Mütəxəssislərimiz plastik məhsullar ilə nece davranmalı
                                olduğunu bilir və bununla plastik dünyasında yeni bir dövr açır.
                            </p>
                        </div>
                    </div>
                </div>
                <button><img src="img/arrow_about.svg" alt=""><span>Ətraflı</span></button>
            </div>
        </div>
      </div>
      <div class="swiper-pagination"></div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>


<?php
    include("includes/script.php");
?>