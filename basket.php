<?php
    include("includes/head.php");
?>


<section class="basket">
    <?php
        include("includes/header.php");
    ?>
    <div class="basket_container position-relative">
      <div class="container breadcrumps_container">
        <div class="row">
          <div class="breadcrumps">
            <a href="index.php" class="old_page">Məhsullar</a>
            <img src="img/breadcrump.svg" alt="">
            <span class="current_page">Papuc - Pəncərə üçün</span>
          </div>
        </div>
      </div>
      <div class="basket_first w-100 position-relative">
        <div class="container">
          <div class="row">
            <div class="right_fixed_total_container">
              <div class="title_total_box">
                <div class="heading">Ümumi məbləğ</div>
                <p>
                  <span id="total"></span><img src="img/manat.svg" alt="">
                </p>
              </div>
              <a href="pay.php" class="pay_btn">Ödənişi tamamla</a>
            </div>
            <div class="basket_box w-100">
              <div class="basket-single">
                <div class="basket_img"><img src="img/probka1.svg" alt=""></div>
                <div class="basket_content_box">
                  <div class="basket_content_title">
                    <p class="title">Elektrik montaj qutusu</p>
                    <p class="money_price" ><span data-price="0.2">0.2</span><img src="img/manat.svg" alt=""></p>
                  </div>
                  <div class="basket_operation">
                    <div class="basket_count_box" data-target="amount-1">
                      <button class="cart-minus-1">-</button>
                      <input type="number" id="amount-1" value="1" name="" min="1">
                      <button class="cart-plus-1">+</button>
                    </div>
                    <div class="basket_color_box">
                      <div class="basket_main basket_same_color_box" style="background-color: #1A3046"></div>
                      <div class="basket_body">
                        <div class="basket_same_color_box" style="background-color: #D5D5D5"></div>
                        <div class="basket_same_color_box" style="background-color: #DEDEDE"></div>
                        <div class="basket_same_color_box" style="background-color: #5A5A5A"></div>
                        <div class="basket_same_color_box" style="background-color: #F0F0F0"></div>
                      </div>
                      <button class="basket_right_btn">
                        <img src="img/basket_arrow.svg" alt="">
                      </button>
                    </div>
                    <div class="basket_size_box">
                      <div class="basket_main basket_same_size_box">15 mm</div>
                      <div class="basket_body">
                        <div class="basket_same_size_box">5 mm</div>
                        <div class="basket_same_size_box">8 mm</div>
                        <div class="basket_same_size_box">9 mm</div>
                        <div class="basket_same_size_box">6 mm</div>
                      </div>
                      <button class="basket_right_btn">
                        <img src="img/basket_arrow.svg" alt="">
                      </button>
                    </div>
                    <button class="delete">
                      <img src="img/delete.svg" alt="">
                    </button>
                  </div>
                </div>
              </div>
              <div class="basket-single">
                <div class="basket_img"><img src="img/probka1.svg" alt=""></div>
                <div class="basket_content_box">
                  <div class="basket_content_title">
                    <p class="title">Elektrik montaj qutusu</p>
                    <p class="money_price" ><span data-price="0.2">0.2</span><img src="img/manat.svg" alt=""></p>
                  </div>
                  <div class="basket_operation">
                    <div class="basket_count_box" data-target="amount-2">
                      <button class="cart-minus-1">-</button>
                      <input type="number" id="amount-2" value="1" name="" min="1">
                      <button class="cart-plus-1">+</button>
                    </div>
                    <div class="basket_color_box">
                      <div class="basket_main basket_same_color_box" style="background-color: #1A3046"></div>
                      <div class="basket_body">
                        <div class="basket_same_color_box" style="background-color: #D5D5D5"></div>
                        <div class="basket_same_color_box" style="background-color: #DEDEDE"></div>
                        <div class="basket_same_color_box" style="background-color: #5A5A5A"></div>
                        <div class="basket_same_color_box" style="background-color: #F0F0F0"></div>
                      </div>
                      <button class="basket_right_btn">
                        <img src="img/basket_arrow.svg" alt="">
                      </button>
                    </div>
                    <div class="basket_size_box">
                      <div class="basket_main basket_same_size_box">15 mm</div>
                      <div class="basket_body">
                        <div class="basket_same_size_box">5 mm</div>
                        <div class="basket_same_size_box">8 mm</div>
                        <div class="basket_same_size_box">9 mm</div>
                        <div class="basket_same_size_box">6 mm</div>
                      </div>
                      <button class="basket_right_btn">
                        <img src="img/basket_arrow.svg" alt="">
                      </button>
                    </div>
                    <button class="delete">
                      <img src="img/delete.svg" alt="">
                    </button>
                  </div>
                </div>
              </div>
              <div class="basket-single">
                <div class="basket_img"><img src="img/probka1.svg" alt=""></div>
                <div class="basket_content_box">
                  <div class="basket_content_title">
                    <p class="title">Elektrik montaj qutusu</p>
                    <p class="money_price" ><span data-price="0.2">0.2</span><img src="img/manat.svg" alt=""></p>
                  </div>
                  <div class="basket_operation">
                    <div class="basket_count_box" data-target="amount-3">
                      <button class="cart-minus-1">-</button>
                      <input type="number" id="amount-3" value="1" name="" min="1">
                      <button class="cart-plus-1">+</button>
                    </div>
                    <div class="basket_color_box">
                      <div class="basket_main basket_same_color_box" style="background-color: #1A3046"></div>
                      <div class="basket_body">
                        <div class="basket_same_color_box" style="background-color: #D5D5D5"></div>
                        <div class="basket_same_color_box" style="background-color: #DEDEDE"></div>
                        <div class="basket_same_color_box" style="background-color: #5A5A5A"></div>
                        <div class="basket_same_color_box" style="background-color: #F0F0F0"></div>
                      </div>
                      <button class="basket_right_btn">
                        <img src="img/basket_arrow.svg" alt="">
                      </button>
                    </div>
                    <div class="basket_size_box">
                      <div class="basket_main basket_same_size_box">15 mm</div>
                      <div class="basket_body">
                        <div class="basket_same_size_box">5 mm</div>
                        <div class="basket_same_size_box">8 mm</div>
                        <div class="basket_same_size_box">9 mm</div>
                        <div class="basket_same_size_box">6 mm</div>
                      </div>
                      <button class="basket_right_btn">
                        <img src="img/basket_arrow.svg" alt="">
                      </button>
                    </div>
                    <button class="delete">
                      <img src="img/delete.svg" alt="">
                    </button>
                  </div>
                </div>
              </div>
              <div class="basket-single">
                <div class="basket_img"><img src="img/probka1.svg" alt=""></div>
                <div class="basket_content_box">
                  <div class="basket_content_title">
                    <p class="title">Elektrik montaj qutusu</p>
                    <p class="money_price" ><span data-price="0.2">0.2</span><img src="img/manat.svg" alt=""></p>
                  </div>
                  <div class="basket_operation">
                    <div class="basket_count_box" data-target="amount-4">
                      <button class="cart-minus-1">-</button>
                      <input type="number" id="amount-4" value="1" name="" min="1">
                      <button class="cart-plus-1">+</button>
                    </div>
                    <div class="basket_color_box">
                      <div class="basket_main basket_same_color_box" style="background-color: #1A3046"></div>
                      <div class="basket_body">
                        <div class="basket_same_color_box" style="background-color: #D5D5D5"></div>
                        <div class="basket_same_color_box" style="background-color: #DEDEDE"></div>
                        <div class="basket_same_color_box" style="background-color: #5A5A5A"></div>
                        <div class="basket_same_color_box" style="background-color: #F0F0F0"></div>
                      </div>
                      <button class="basket_right_btn">
                        <img src="img/basket_arrow.svg" alt="">
                      </button>
                    </div>
                    <div class="basket_size_box">
                      <div class="basket_main basket_same_size_box">15 mm</div>
                      <div class="basket_body">
                        <div class="basket_same_size_box">5 mm</div>
                        <div class="basket_same_size_box">8 mm</div>
                        <div class="basket_same_size_box">9 mm</div>
                        <div class="basket_same_size_box">6 mm</div>
                      </div>
                      <button class="basket_right_btn">
                        <img src="img/basket_arrow.svg" alt="">
                      </button>
                    </div>
                    <button class="delete">
                      <img src="img/delete.svg" alt="">
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <div class="container">
        <div class="row">
          <ul class="pagination_project">
            <li><a href="#" class="active">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
          </ul>
          <div class="similar_container w-100">
            <p class="similar_title">OXŞAR MƏHSULLAR</p>
            <div class="similiar_box">
              <div class="similiar_single">
                <div class="similiar_content_title">
                  <p>Elektrik montaj qutusu</p>
                  <span>50<img src="img/manat.svg" alt=""></span>
                </div>
                <div class="sim_single_img"><img src="img/similiar1.svg" alt=""></div>
              </div>
              <div class="similiar_single">
                <div class="similiar_content_title">
                  <p>Elektrik montaj qutusu</p>
                  <span>50<img src="img/manat.svg" alt=""></span>
                </div>
                <div class="sim_single_img"><img src="img/similiar2.svg" alt=""></div>
              </div>
              <div class="similiar_single">
                <div class="similiar_content_title">
                  <p>Elektrik montaj qutusu</p>
                  <span>50<img src="img/manat.svg" alt=""></span>
                </div>
                <div class="sim_single_img"><img src="img/similiar3.svg" alt=""></div>
              </div>
              <div class="similiar_single">
                <div class="similiar_content_title">
                  <p>Elektrik montaj qutusu</p>
                  <span>50<img src="img/manat.svg" alt=""></span>
                </div>
                <div class="sim_single_img"><img src="img/similiar1.svg" alt=""></div>
              </div>
              <div class="similiar_single">
                <div class="similiar_content_title">
                  <p>Elektrik montaj qutusu</p>
                  <span>50<img src="img/manat.svg" alt=""></span>
                </div>
                <div class="sim_single_img"><img src="img/similiar2.svg" alt=""></div>
              </div>
              <div class="similiar_single">
                <div class="similiar_content_title">
                  <p>Elektrik montaj qutusu</p>
                  <span>50<img src="img/manat.svg" alt=""></span>
                </div>
                <div class="sim_single_img"><img src="img/similiar3.svg" alt=""></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>


<?php
    include("includes/script.php");
?>