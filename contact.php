<?php
    include("includes/head.php");
?>


<section class="contact">
    <?php
        include("includes/header.php");
    ?>
    <div class="container">
      <div class="row">
        <div class="contact_inner position-relative w-100">
          <div class="breadcrumps">
            <a href="index.php" class="old_page">Məhsullar</a>
            <img src="img/breadcrump.svg" alt="">
            <span class="current_page">Papuc - Pəncərə üçün</span>
          </div>
          <div class="contact_title">Bizimlə əlaqə saxlayın!</div>
          <div class="contact_form_box">
            <div id="map"></div>
            <form action="">
              <input type="text" placeholder="Adınız" required>
              <input type="text" placeholder="Soyadınız" required>
              <input type="email" placeholder="Email" required>
              <div class="country_input">
                <div class="country_flag">
                  <div class="country_text">
                    <img src="img/aze.svg" alt="" class="flag_img">
                    <span>+994</span>
                  </div>
                  <div class="country_absolute_flags">
                    <div class="flag_box flag_box1">
                      <img src="img/aze.svg" alt="" class="flag_img">
                      <span>+994</span>
                    </div>
                    <div class="flag_box flag_box1">
                      <img src="img/gb.svg" alt="" class="flag_img">
                      <span>+1122</span>
                    </div>
                    <div class="flag_box flag_box1">
                      <img src="img/usa.svg" alt="" class="flag_img">
                      <span>+5369</span>
                    </div>
                  </div>

                </div>
                <input type="number" placeholder="Mobil nömrə" required>
              </div>
              <textarea placeholder="Bizə yazın......" required></textarea>
              <button type="submit">GÖNDƏR</button>
            </form>
          </div>
          <div class="contact_info">
            <div class="contact_single" data-aos="fade-up" data-aos-duration="600" data-aos-easing="ease-in-out">
              <div class="contact_img"><img src="img/gif/mail.gif" alt=""></div>
              <div class="contact_desc">
                <p>polemak@gmail.com</p>
                <p>polemak1970@gmail.com</p>
              </div>
            </div>
            <div class="contact_single" data-aos="fade-up" data-aos-duration="700" data-aos-easing="ease-in-out">
              <div class="contact_img"><img src="img/gif/phone.gif" alt=""></div>
              <div class="contact_desc">
                <p>Sədərək şöbəsi </p>
                <p>+(994)55 759 10 30</p>
                <p>+(994)55 277 55 77</p>
                <p>+(994)70 266 02 88y</p>
              </div>
            </div>
            <div class="contact_single" data-aos="fade-up" data-aos-duration="800" data-aos-easing="ease-in-out">
              <div class="contact_img">
              <video autoplay loop muted  >
                  <source  src="img/gif/location.gif">
              </video>
              </div>
              <div class="contact_desc">
                <p>Factories arose with the introduction of machinery</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<script  src="js/map.js"></script>
<script  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4SgjsTpN_D4nuu-O2Dn8f5aeK7dLjwoQ&callback=initMap&libraries=places" async defer></script>


<?php
    include("includes/script.php");
?>