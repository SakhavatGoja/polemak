<?php
    include("includes/head.php");
?>


<section class="galery">
    <?php
        include("includes/header.php");
    ?>
    <div class="galery_main_container">
      <div class="swiper-container gallery-top">
        <div class="swiper-wrapper">
            <div class="swiper-slide"><div class="swiper-slide-container"><img src="img/galery_back.svg" alt=""></div></div>
            <div class="swiper-slide"><div class="swiper-slide-container"></div></div>
            <div class="swiper-slide"><div class="swiper-slide-container"><img src="img/galery_back.svg" alt=""></div></div>
            <div class="swiper-slide"><div class="swiper-slide-container"><img src="img/galery_back.svg" alt=""></div></div>
        </div>
      </div>
      <div class="swiper-container gallery-thumbs">
          <div class="swiper-wrapper">
              <div class="swiper-slide">
                <div class="swiper-slide-container">
                  <img src="img/galery1.svg" alt="">
                </div>
              </div>
              <div class="swiper-slide">
                  <div class="swiper-slide-container">
                      <img src="img/galery2.svg" alt="">
                  </div>
              </div>
              <div class="swiper-slide">
                <div class="swiper-slide-container">
                    <img src="img/galery3.svg" alt="">
                </div>
              </div>
              <div class="swiper-slide">
                <div class="swiper-slide-container">
                    <img src="img/galery4.svg" alt="">
                </div>
              </div>
              
              <!-- Add Arrows -->
          </div>
          <div class="left_arrow arrow_swiper"><img src="img/left_swiper.svg" alt=""></div>
          <div class="right_arrow arrow_swiper"><img src="img/right_swiper.svg" alt=""></div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>


<?php
    include("includes/script.php");
?>