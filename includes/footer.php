<footer data-aos="fade-in" data-aos-duration="1200" data-aos-easing="ease-in-out">
  <div class="footer_max_container">
    <div class="container">
      <div class="row">
        <div class="footer_container w-100">
          <div class="footer_left">
            <ul>
              <li><a href="#">Haqqımızda</a></li>
              <li><a href="#">Məhsullar</a></li>
              <li><a href="#">Qalereya</a></li>
              <li><a href="#">Rəylər</a></li>
              <li><a href="#">Əlaqə</a></li>
            </ul>
          </div>
          
          <div class="footer_right">
            <div class="footer_socials">
              <a href="#"><img src="img/insta.svg" alt=""></a>
              <a href="#"><img src="img/fb.svg" alt=""></a>
              <a href="#"><img src="img/wp.svg" alt=""></a>
            </div>
            <div class="info_footer_right">
              <p>Sədərək şöbəsi </p>
              <p>+(994)55 277 55 77</p>
              <p>+(994)55 759 10 30</p>
              <p>+(994)70 266 02 88</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>