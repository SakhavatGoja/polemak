<header>
  <div class="container">
    <div class="row">
      <div class="header_container">
          <a href="index.php" class="logo">
            <img src="img/logo.svg" alt="">
          </a>
          <ul>
            <li><a href="about.php">HAQQIMIZDA</a></li>
            <li><a href="projects.php">MƏHSULLAR</a></li>
            <li><a href="galery.php">QALEREYA</a></li>
            <li><a href="reviews.php">RƏYLƏR</a></li>
            <li><a href="contact.php">ƏLAQƏ</a></li>
          </ul>
          <div class="header_right">
            <div class="search-bar">
              <span class="search-icon"><img src="img/search.svg" alt=""></span>
              <form class="search" action="">
                <input class="search" type="text" placeholder="Search"/>
                <button type="submit" class="btn">axtar</button>
              </form>
            </div>
            <a href="#"><img src="img/location.svg" alt=""></a>
            <a href="basket.php"><img src="img/shop.svg" alt=""></a>
          </div>
          <div class="button_container" id="toggle">
            <span class="first"></span>
            <span class="second"></span>
            <span class="third"></span>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>

<div class="overlay" id="overlay">
  <div class="overlay_header">
      <div class="header_right">
          <div class="search-bar">
            <span class="search-icon"><img src="img/search.svg" alt=""></span>
            <form class="search" action="">
              <input class="search" type="text" placeholder="Search"/>
              <button type="submit" class="btn">axtar</button>
            </form>
          </div>
          <a href="#"  class="link_overlay"><img src="img/location.svg" alt=""></a>
          <a href="basket.php"  class="link_overlay"><img src="img/shop.svg" alt=""></a>
      </div>
      <div class="button_container active">
        <span class="first"></span>
        <span class="second"></span>
        <span class="third"></span>
      </div>
  </div>
  <nav class="overlay-menu">
    <ul>
      <li ><a href="about.php">HAQQIMIZDA</a></li>
      <li><a href="projects.php">MƏHSULLAR</a></li>
      <li><a href="galery.php">QALEREYA</a></li>
      <li><a href="reviews.php">RƏYLƏR</a></li>
      <li><a href="contact">ƏLAQƏ</a></li>
    </ul>
  </nav>
</div>
