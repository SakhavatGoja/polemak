<?php
    include("includes/head.php");
?>


<section class="index">
    <?php
        include("includes/header.php");
    ?>
    <div class="index_landing">
        <div id="animatedBackground"></div>
        <div class="container">
            <div class="row">
                <div class="index_landing_inner w-100">
                    <div class="landing_left_info">
                        <h1 class="landing_title">POLEMAK</h1>
                        <div class="landing_info_box">
                            <p>Keyfiyyətin kəmiyyətdən üstünlüyü.</p>
                            <p>Factory, manufacturing plant or a production. lorem</p>
                        </div>
                        <a href="#" class="more">Daha ətraflı</a>
                    </div>
                    <div class="landing_right_gif">
                        <img src="img/webp/polemak.webp" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="arrow">
            <span></span>
            <span></span>
            <span></span>
            <p>Sürüşdür</p>
        </div>
    </div>
    <div class="index_about ">
        <div class="layer_blur"></div>
        <div class="index_about_container">
            <div class="container">
                <div class="row">
                    <div class="about_text_box w-100">
                        <p class="index_container_title">Məhsullar</p>
                        <div class="about_text">
                            Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting
                            of several into another. They are a critical part of modern economic production, with the majority.
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-container index_about_swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="swiper_img_box">
                            <img src="img/about_img_1.svg" alt="">
                            
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper_img_box">
                            <img src="img/about_img_2.svg" alt="">
                            
                        </div>
                        
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper_img_box">
                            <img src="img/about_img_3.svg" alt="">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper_img_box">
                            <img src="img/about_img_4.svg" alt="">
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="swiper_img_box">
                            <img src="img/about_img_5.svg" alt="">
                        </div>
                    </div>
                </div>
                <div class="left_arrow arrow_swiper"><img src="img/left_swiper.svg" alt=""></div>
                <div class="right_arrow arrow_swiper"><img src="img/right_swiper.svg" alt=""></div>
            </div>
        </div>
    </div>
    <div class="index_projects">
        <div class="container">
            <div class="row">
                <div class="index_projects_container">
                    <p class="index_container_title">Məhsullar</p>
                    <div class="index_pro_title_container w-100">
                        <div class="elektrik_box">
                            <p class="eletrik_title">Elektrik montaj qutusu</p>
                            <p class="elektrik_body">
                                Factory, manufacturing plant or a production plant is an industrial site, often a complex
                                 consisting of several buildings
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-container project_swiper">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <div class="swiper-slide-inner">
                        <div class="main_img"><img src="img/project_swiper.svg" alt=""></div>
                        <a href="#" class="swiper_prject_more">
                            <span>Daha ətraflı</span>
                            <img src="img/more_swiper.svg" alt="">
                        </a>
                        <div class="size_project">
                            <p>Ölçü</p>
                            <div class="size_box">
                                <div>15 mm</div>
                                <div>8 mm</div>
                            </div>
                        </div>
                        <div class="color_project">
                            <p>Rənglər</p>
                            <div class="color_box">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="swiper-slide-inner">
                        <div class="main_img"><img src="img/project_swiper2.svg" alt=""></div>
                        <a href="#" class="swiper_prject_more">
                            <span>Daha ətraflı</span>
                            <img src="img/more_swiper.svg" alt="">
                        </a>
                        <div class="size_project">
                            <p>Ölçü</p>
                            <div class="size_box">
                                <div>9 mm</div>
                                <div>3 mm</div>
                            </div>
                        </div>
                        <div class="color_project">
                            <p>Rənglər</p>
                            <div class="color_box">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="swiper-slide-inner">
                        <div class="main_img"><img src="img/project_swiper3.svg" alt=""></div>
                        <a href="#" class="swiper_prject_more">
                            <span>Daha ətraflı</span>
                            <img src="img/more_swiper.svg" alt="">
                        </a>
                        <div class="size_project">
                            <p>Ölçü</p>
                            <div class="size_box">
                                <div>7 mm</div>
                                <div>3 mm</div>
                            </div>
                        </div>
                        <div class="color_project">
                            <p>Rənglər</p>
                            <div class="color_box">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="left_arrow arrow_swiper"><img src="img/left_swiper.svg" alt=""></div>
            <div class="right_arrow arrow_swiper"><img src="img/right_swiper.svg" alt=""></div>
        </div>
    </div>
    <div class="index_electric">
        <div class="index_electric_container">
            <div class="electric_box electric_left w-100">
                <div class="img_electric"><img src="img/project_swiper2.svg" alt=""></div>
                <div class="container">
                    <div class="row">
                        <div class="electric_inner_box">
                            <button>
                                <div>15 mm</div>
                            </button>
                            <div class="electric_body">
                                <p class="electric_title">Elektrik montaj qutusu</p>
                                <p class="electric_info">
                                    Factory, manufacturing plant or a production plant is an industrial site,
                                    often a complex consisting of several buildings 
                                </p>
                                <a href="#" class="el_arrow"><img src="img/electric_arrow.svg" alt=""></a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="electric_box electric_right w-100">
                <div class="img_electric"><img src="img/project_swiper3.svg" alt=""></div>
                <div class="container">
                    <div class="row">
                        <div class="electric_inner_box">
                            <button>
                                <div>15 mm</div>
                            </button>
                            <div class="electric_body">
                                <p class="electric_title">Elektrik montaj qutusu</p>
                                <p class="electric_info">
                                    Factory, manufacturing plant or a production plant is an industrial site,
                                    often a complex consisting of several buildings 
                                </p>
                                <a href="#" class="el_arrow"><img src="img/electric_arrow.svg" alt=""></a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="index_montaj">
        <div class="montaj_img"><img src="img/montaj.svg" alt=""></div>
        <div class="container">
            <div class="row">
                <div class="montaj_inner_box">
                    <p class="montaj_title">Elektrik montaj qutusu</p>
                    <div class="montaj_body">
                        <p>
                            Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several buildings
                             filled with machinery,where workers manufacture items or operate machines which process each item into another.
                             <br>
                             <br>
                             Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several buildings filled with machinery, where workers manufacture items or operate machines which process each item into another. 
                        </p>
                        <a href="#"><span>Elektrik montaj qutusu</span><img src="img/more_swiper.svg" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="reviews_container">
        <div class="container first_cont">
            <div class="row">
                <div class="about_text_box w-100">
                    <p class="index_container_title">Rəylər</p>
                </div>
            </div>
        </div>
        <div class="review_single w-100">
            <div class="swiper-container review_swiper">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="rev_img"><img src="img/review1.jpg" alt=""></div>
                        <div class="container">
                            <div class="row">
                                <div class="stars_box">
                                    <p class="company_name">Polemak 1</p>
                                    <p class="company_info">
                                        Production plant is an industrial site, often a complex consisting of several buildings filled with machinery.
                                        Production plant is an industrial site, often a complex consisting of several buildings filled with machinery.
                                    </p>
                                    <div class="stars review5"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="rev_img"><img src="img/review1.jpg" alt=""></div>
                        <div class="container">
                            <div class="row">
                                <div class="stars_box">
                                    <p class="company_name">Polemak 2</p>
                                    <p class="company_info">Production plant is an industrial site, often a complex consisting of several buildings filled with machinery.</p>
                                    <div class="stars review5"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="rev_img"><img src="img/review1.jpg" alt=""></div>
                        <div class="container">
                            <div class="row">
                                <div class="stars_box">
                                    <p class="company_name">Polemak 3</p>
                                    <p class="company_info">Production plant is an industrial site, often a complex consisting of several buildings filled with machinery.</p>
                                    <div class="stars review5"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="left_arrow arrow_swiper"><img src="img/left_swiper.svg" alt=""></div>
                <div class="right_arrow arrow_swiper"><img src="img/right_swiper.svg" alt=""></div>
            </div>
            
        </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>
