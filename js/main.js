AOS.init();

$('.project_main button').on('click',function(){
  $('.project_bottom.show').removeClass('show');
  $(this).parent().next().addClass('show');
  $('.project_main button').removeClass('hide');
  $(this).addClass('hide');
})

$('.project_bottom .quit').on('click',function(){
  $(this).parent().removeClass('show');
  $(this).parent().prev().find(' button').removeClass('hide')
})

$('.price_tabs button').on('click', function(){
  var currentNewsValue = parseInt($(this).attr('data-id'));
  
  $('.project_tab_box').hide();
  if(!$(this).hasClass('rotate')){
    $(this).siblings().removeClass('rotate')
    $(this).addClass('rotate');
    $(this).parents('.project_bottom').find(`.project_tab_box[data-tab='${currentNewsValue}']`).fadeIn(500);
  }
  else{
    $(this).removeClass('rotate')
    $(this).parents('.project_bottom').find(`.project_tab_box[data-tab='${currentNewsValue}']`).fadeOut(500)
  }
})

$('.country_text').on('click',function(){
  $('.country_absolute_flags').toggleClass('show');
  $(this).toggleClass('rotate');
})

$('.flag_box').on('click' ,function(){
  $('.country_text').html($(this).html());
  $('.country_text').removeClass('rotate');
})

$('body').on('click', function (e) {
    if ($('.country_text').length !== 0 && !$(e.target).hasClass('country_text') && !$(e.target).parent().hasClass('country_text')){
        $('.country_absolute_flags').removeClass('show');
        $('.country_text').removeClass('rotate');
    }
});

$('.about .swiper-slide button').on('click', function(){
  $(this).parent().find('.content').toggleClass('show')
  $(this).toggleClass('rotate')
  $(this).parents('body').find('.swiper-pagination').toggleClass('show')
})
// $('.swiper-pagination-bullet').on('click' , function(){
//   $(this).parent().removeClass('show')
// })

$('#toggle').on('click',(function() {
  $('#overlay').addClass('open');
  $('body').addClass('not_scroll')
 }));

 $('#overlay .button_container').on('click', function(){
  $('#overlay').removeClass('open');
  $('body').removeClass('not_scroll')
 })

sum();

function sum(){
  var total = 0;
  $('.basket_content_box .money_price').each(function(){
    total += +parseFloat($(this).text());
    console.log(total)
  });
    
  $("#total").text(total.toFixed(2));
}

var cartButtons = $('.basket_count_box').find('button');

$(cartButtons).on('click', function(e) {
  e.preventDefault();
  var $this = $(this);
  let $item = $(this).parents('.basket_content_box').find('.money_price').find('span');
  let $itemPrice = parseFloat($item.data('price'))
  var target = $this.parent().data('target');
  var target = $('#' + target);
  var current = parseFloat($(target).val());
  if ($this.hasClass('cart-plus-1')){
        parseFloat(target.val(current + 1)).toFixed(2);
        $item.text(parseFloat($itemPrice * target.val()).toFixed(2))
      }
  else {
    if(current < 2){
      return null
    }
    else{
      target.val(current - 1);
      $item.text(parseFloat($itemPrice * target.val()).toFixed(2))
    }
  }

  sum()
  
});

$('.basket_count_box input').on('input', function(e) {
  var $this = $(this);
  let $item = $(this).parents('.basket_content_box').find('.money_price').find('span');
  let $itemPrice = parseFloat($item.data('price'))
  var target = $this.parents('.basket_count_box').data('target');
  var target = $('#' + target);
  var current = parseFloat($(target).val());
  $item.text(parseFloat($itemPrice * target.val()).toFixed(2))
  sum();
});

$('.basket-single .delete').on('click',function(){
  
  let $parent = $(this).parents('.basket-single');

  $parent.fadeOut(300, function(){
       $parent.remove();
       sum();
  })
  if($('.basket-single').length < 2) setTimeout(() => {
    $('.right_fixed_total_container').remove()
  }, 300);

})


$(window).on('scroll', function() {
  if ($('.basket_first').length && $(window).scrollTop() >=
   $('.basket_first').offset().top + $('.basket_first').outerHeight() - window.innerHeight + 100) {
      $('.right_fixed_total_container').addClass('not_fixed')
  }
  else{
    $('.right_fixed_total_container').removeClass('not_fixed')
  }
});

$('.basket_right_btn').on('click', function(){
  $(this).parent().find('.basket_body').toggleClass('opened');
  $(this).toggleClass('rotate');
})

$('.basket_same_color_box').on('click' , function(){
  let parent = $(this).parents('.basket_color_box')
  let choosenColor = $(this).css('background-color');
  let mainColor = parent.find('.basket_main').css('background-color');
  parent.find('.basket_main').css('background-color', choosenColor)
  $(this).css('background-color' , mainColor);
  parent.find('.basket_body').toggleClass('opened');
  parent.find('.basket_right_btn').toggleClass('rotate');
})

$('.basket_same_size_box').on('click' , function(){
  let parent = $(this).parents('.basket_size_box')
  let choosenText = $(this).text();
  let mainText = parent.find('.basket_main').text();
  parent.find('.basket_main').text(choosenText)
  $(this).text(mainText);
  parent.find('.basket_body').toggleClass('opened');
  parent.find('.basket_right_btn').toggleClass('rotate');
})


var icon = $('.search-icon');
var form = $('form.search');

icon.click(function() {
  $(this).toggleClass('left')
  form.toggleClass('open');
  if($(this).parents('.overlay')){
    console.log('asdasdasd')
    $('.link_overlay').toggleClass('link_mobile')
  }
  let flag = $(this).parents('.header_container').find('ul').hasClass('none')
  if(!flag){
    $(this).parents('.header_container').find('ul').addClass('none')
  }
  else{
    setTimeout(() => {
      $(this).parents('.header_container').find('ul').removeClass('none')
    }, 500);
  }
  if (form.hasClass('open')) {
    form.children('input.search').focus();
  }
  
});

let indexButton = $('.electric_box .electric_inner_box button')
var $w = $(window).scroll(function(){
    for (i = 0; i < indexButton.length; i++) {
      const element = $(indexButton[i]).offset().top - 60;
        if ( $w.scrollTop() > element ) {   
          $(indexButton[i]).addClass('change')
          $(indexButton[i]).addClass('change')
        } else {
        }
    }
});

$('.color_link').on('click',function(e){
  e.preventDefault();
  $('.color_link').removeClass('active');
  $(this).addClass('active');
})

$('.size_link').on('click',function(e){
  e.preventDefault();
  $('.size_link').removeClass('active');
  $(this).addClass('active');
})


