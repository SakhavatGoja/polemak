// about-swiper-start
var swiper = new Swiper('.about .swiper-container', {
  slidesPerView: 1,
  slidesPerGroup: 1,
  spaceBetween: 0,
  loop: true,
  mousewheel: true,
  loopFillGroupWithBlank: false,
  speed: 750,
  effect: 'fade',
  fadeEffect: {
    crossFade: true
  },
  autoplay: {
      delay: 7000,
      disableOnInteraction: false,
  },
  pagination: {
    el: ".swiper-pagination",
    clickable: true
  },
});


// about-swiper-end

// galery-slider-start
var galleryTop = new Swiper('.gallery-top', {
  spac0eBetween: 1,
   effect: 'fade',
    fadeEffect: {
      crossFade: true
    },
   loop: true,
  loopedSlides: 4
});

var galleryThumbs = new Swiper('.gallery-thumbs', {
  slidesPerView: 4,
  slidesPerGroup: 1,
  navigation: {
  nextEl: '.gallery-thumbs .right_arrow',
  prevEl: '.gallery-thumbs .left_arrow',
  },
  touchRatio: 0.2,
  slideToClickedSlide: true,
  loop: true,
  breakpoints: {
    320: {
      spaceBetween: 4,
      centeredSlides: true
    },
    768: {
      spaceBetween: 19,
    }
  }
});

if($('.gallery-top').length){
  galleryTop.controller.control = galleryThumbs ;
  galleryThumbs.controller.control = galleryTop ;
}
// galery-slider-end


// index-about-swiper-start
var about_swiper = new Swiper('.index_about_swiper', {
  centeredSlides: true,
  grabCursor: true,
  slideToClickedSlide: true,
  speed: 750,
  autoplay: {
      delay: 7000,
      disableOnInteraction: false,
  },
  loop: true,
  // mousewheel: true,
  navigation: {
    nextEl: '.index_about .right_arrow',
    prevEl: '.index_about .left_arrow',
  },
  breakpoints: {
    320: {
      slidesPerView: 2,
      slidesPerGroup: 1,
      spaceBetween: 7
    },
    768: {
      slidesPerView: 3,
      slidesPerGroup: 1,
      spaceBetween: 6,
    }
  }
});
// index-about-swiper-end

// project-swiper-start
var project_swiper = new Swiper('.project_swiper', {
  slidesPerView: 1,
  slidesPerGroup: 1,
  grabCursor: true,
  speed: 750,
  autoplay: {
      delay: 7000,
      disableOnInteraction: false,
  },
  effect: 'fade',
  fadeEffect: {
    crossFade: true
  },
  loop: true,
  // mousewheel: true,
  navigation: {
    nextEl: '.project_swiper .right_arrow',
    prevEl: '.project_swiper .left_arrow',
  }
});
// project-swiper-end

// review-swiper-start
var review_swiper = new Swiper('.review_swiper', {
  slidesPerView: 1,
  slidesPerGroup: 1,
  grabCursor: true,
  speed: 750,
  autoplay: {
      delay: 7000,
      disableOnInteraction: false,
  },
  effect: 'fade',
  fadeEffect: {
    crossFade: true
  },
  loop: true,
  // mousewheel: true,
  navigation: {
    nextEl: '.review_swiper .right_arrow',
    prevEl: '.review_swiper .left_arrow',
  }
});
// review-swiper-end