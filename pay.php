<?php
    include("includes/head.php");
?>


<section class="pay">
    <?php
        include("includes/header.php");
    ?>
    <div class="pay_container">
      <div class="pay_form_box">
        <div class="form_common_price">
          <p>Ümumi məbləğ</p>
          <div>600 <img src="img/manat.svg" alt=""></div>
        </div>
        <form id="pay_form" action="#">
          <div class="form-group" data-required="*">
            <input type="text" name="name" placeholder='Adınız' required>
          </div>
          <div class="form-group" data-required="*">
            <input type="text" name="surname" placeholder='Soyadınız' required>
          </div>
          <div class="form-group" data-required="*">
            <input type="email" name="email" placeholder='Email' required>
          </div>
          <div class="number_box">
            <div class="form-group">
              <div class="select_container">
                <select name="prefix" class="nice-select">
                    <option value="0" selected>+994</option>
                    <option value="1">+987</option>
                    <option value="2">+954</option>
                </select>
              </div>
            </div>
            <div class="form-group" data-required="*">
              <input type="number" name="number" placeholder='Nömrə' required>
            </div>
          </div>
          <div class="form-group">
            <textarea name="textarea" placeholder="Ünvan" required></textarea>
          </div>
          <div class="select_box">
            <div class="form-group" data-required="*">
                  <select name="city[]" class="nice-select" required>
                      <option value= "" disabled selected>Şəhər</option>
                      <option value="1">Bakı</option>
                      <option value="2">Sumqayıt</option>
                      <option value="3">Gəncə</option>
                      <option value="4">Mingəçevir</option>
                  </select>
            </div>
            <div class="form-group" data-required="*">
                  <select name="district[]" class="nice-select" required>
                      <option value= "" disabled selected>Rayon</option>
                      <option value="1">Bakı</option>
                      <option value="2">Sumqayıt</option>
                      <option value="3">Gəncə</option>
                      <option value="4">Mingəçevir</option>
                  </select>
            </div>
          </div>
          <div class="pay_button_container">
            <p class="title_pay_container">Ödəniş üsulunu seçin</p>
            <div class="pay_btn_box">
              <div class="form-group"><button type="submit">Nağd</button></div>
              <div class="form-group"><button type="submit">Onlayn</button></div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div class="modal fade">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="success_content">
            <p>Sifarişiniz uğurla tamamlandı!</p>
            <span>Sizinlə əlaqə saxlanılacaq</span>
          </div>
          <div class="unsuccess_content">
            <p>Əməliyyat  uğursuzdur</p>
          </div>
        </div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>

<?php
    include("includes/script.php");
?>