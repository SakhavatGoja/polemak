<?php
    include("includes/head.php");
?>


<section class="project_inner">
    <?php
        include("includes/header.php");
    ?>
    <div class="container">
      <div class="row">
        <div class="project_inner_container position-relative w-100">
          <div class="breadcrumps">
            <a href="index.php" class="old_page">Məhsullar</a>
            <img src="img/breadcrump.svg" alt="">
            <span class="current_page">Papuc - Pəncərə üçün</span>
          </div>
          <div class="project_top_container">
            <div class="left_project_box"><img src="img/img_inner.png" alt=""></div>
            <div class="right_project_box">
              <div class="right_heading">
                <p class="heading_project">Papuc - Pəncərə üçün</p>
                <p class="price_project">50 <img src="img/manat_inner.png" alt=""></p>
              </div>
              <div class="about_project">
                <p class="same_project_title">
                  Haqqında
                </p>
                <p class="about_content">
                  Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building.
                   Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building. 
                </p>
              </div>
              <div class="about_color">
                <p class="same_project_title">Rəng</p>
                <div class="colors_box">
                  <a href="#" class="color_link active" style="background-color: #90AFD7"></a>
                  <a href="#" class="color_link" style="background-color: #A1ABB7"></a>
                  <a href="#" class="color_link" style="background-color: #BBD5F9"></a>
                  <a href="#" class="color_link" style="background-color: #D0DAE8"></a>
                  <a href="#" class="color_link" style="background-color: #416AA0"></a>
                </div>
              </div>
              <div class="about_size">
                <p class="same_project_title">Ölçü</p>
                <div class="size_box">
                  <a href="#" class="size_link active">5 mm</a>
                  <a href="#" class="size_link">5 mm</a>
                  <a href="#" class="size_link">5 mm</a>
                  <a href="#" class="size_link">5 mm</a>
                  <a href="#" class="size_link">5 mm</a>
                </div>
              </div>
              <div class="about_btns">
                <button type="button" data-toggle="modal" data-target="#addModal"> + səbətə əlavə et</button>
                <button >birbaşa al</button>
              </div>
            </div>
          </div>
          <div class="similar_container w-100">
            <p class="similar_title">OXŞAR MƏHSULLAR</p>
            <div class="similiar_box">
              <div class="similiar_single">
                <div class="similiar_content_title">
                  <p>Elektrik montaj qutusu</p>
                  <span>50<img src="img/manat.svg" alt=""></span>
                </div>
                <div class="sim_single_img"><img src="img/similiar1.svg" alt=""></div>
              </div>
              <div class="similiar_single">
                <div class="similiar_content_title">
                  <p>Elektrik montaj qutusu</p>
                  <span>50<img src="img/manat.svg" alt=""></span>
                </div>
                <div class="sim_single_img"><img src="img/similiar2.svg" alt=""></div>
              </div>
              <div class="similiar_single">
                <div class="similiar_content_title">
                  <p>Elektrik montaj qutusu</p>
                  <span>50<img src="img/manat.svg" alt=""></span>
                </div>
                <div class="sim_single_img"><img src="img/similiar3.svg" alt=""></div>
              </div>
              <div class="similiar_single">
                <div class="similiar_content_title">
                  <p>Elektrik montaj qutusu</p>
                  <span>50<img src="img/manat.svg" alt=""></span>
                </div>
                <div class="sim_single_img"><img src="img/similiar1.svg" alt=""></div>
              </div>
              <div class="similiar_single">
                <div class="similiar_content_title">
                  <p>Elektrik montaj qutusu</p>
                  <span>50<img src="img/manat.svg" alt=""></span>
                </div>
                <div class="sim_single_img"><img src="img/similiar2.svg" alt=""></div>
              </div>
              <div class="similiar_single">
                <div class="similiar_content_title">
                  <p>Elektrik montaj qutusu</p>
                  <span>50<img src="img/manat.svg" alt=""></span>
                </div>
                <div class="sim_single_img"><img src="img/similiar3.svg" alt=""></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="modal fade modal_basket"  id="addModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="added_project_box">
            <button class="close esc" data-dismiss="modal" aria-hidden="true"><img src="img/esc.svg" alt=""></button>
            <p class="mobile_added_text">Səbətə əlave edildi!</p>
            <div class="add_center_box">
              <div class="added_project">
                <div class="added_img_box"><img src="img/added_img1.svg" alt=""></div>
                <div class="right_added_content">
                  <p class="title">Divar propkası</p>
                  <div class="added_price">125 <img src="img/manat.svg" alt=""></div>
                  <button class="delete"><img src="img/delete.svg" alt=""></button>
                </div>
              </div>
              <div class="added_project">
                <div class="added_img_box"><img src="img/added_img1.svg" alt=""></div>
                <div class="right_added_content">
                  <p class="title">Divar propkası</p>
                  <div class="added_price">125 <img src="img/manat.svg" alt=""></div>
                  <button class="delete"><img src="img/delete.svg" alt=""></button>
                </div>
              </div>
              <div class="added_project">
                <div class="added_img_box"><img src="img/added_img1.svg" alt=""></div>
                <div class="right_added_content">
                  <p class="title">Divar propkası</p>
                  <div class="added_price">125 <img src="img/manat.svg" alt=""></div>
                  <button class="delete"><img src="img/delete.svg" alt=""></button>
                </div>
              </div>
            </div>
            <div class="go_basket_box">
              <a href="basket.php" class="go_basket">Səbətə bax</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <?php
        include("includes/footer.php");
    ?>
</section>



<?php
    include("includes/script.php");
?>
