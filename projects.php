<?php
    include("includes/head.php");
?>


<section class="projects">
    <?php
        include("includes/header.php");
    ?>
    <div class="container">
      <div class="row">
        <div class="projects_container position-relative w-100">
          <div class="breadcrumps">
            <a href="index.php" class="old_page">Məhsullar</a>
            <img src="img/breadcrump.svg" alt="">
            <span class="current_page">Papuc - Pəncərə üçün</span>
          </div>
          <div class="projects_box" id="projects_box" data-columns>
            <div class="project grid-item">
              <div class="project_main">
                <a href="project_inner.php">
                  <img src="img/project.png" alt="">
                </a >
              </div>
            </div>
            <div class="project grid-item">
              <div class="project_main">
                <a href="project_inner.php">
                  <img src="img/project.png" alt="">
                </a >
              </div>
            </div>
            <div class="project grid-item">
              <div class="project_main">
                <a href="project_inner.php">
                  <img src="img/project.png" alt="">
                </a >
              </div>
            </div>
            <div class="project grid-item">
              <div class="project_main">
                <a href="project_inner.php">
                  <img src="img/project.png" alt="">
                </a >
              </div>
            </div>
            <div class="project grid-item">
              <div class="project_main">
                <a href="project_inner.php">
                  <img src="img/project.png" alt="">
                </a >
              </div>
            </div>
            <div class="project grid-item">
              <div class="project_main">
                <a href="project_inner.php">
                  <img src="img/project.png" alt="">
                </a >
              </div>
            </div>
            <div class="project grid-item">
              <div class="project_main">
                <a href="project_inner.php">
                  <img src="img/project.png" alt="">
                </a >
              </div>
            </div>
            <div class="project grid-item">
              <div class="project_main">
                <a href="project_inner.php">
                  <img src="img/project.png" alt="">
                </a >
              </div>
            </div>
          </div>
          <ul class="pagination_project">
            <li><a href="#" class="active">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="modal fade modal_basket"  id="addModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="added_project_box">
            <button class="close esc" data-dismiss="modal" aria-hidden="true"><img src="img/esc.svg" alt=""></button>
            <p class="mobile_added_text">Səbətə əlave edildi!</p>
            <div class="add_center_box">
              <div class="added_project">
                <div class="added_img_box"><img src="img/added_img1.svg" alt=""></div>
                <div class="right_added_content">
                  <p class="title">Divar propkası</p>
                  <div class="added_price">125 <img src="img/manat.svg" alt=""></div>
                  <button class="delete"><img src="img/delete.svg" alt=""></button>
                </div>
              </div>
              <div class="added_project">
                <div class="added_img_box"><img src="img/added_img1.svg" alt=""></div>
                <div class="right_added_content">
                  <p class="title">Divar propkası</p>
                  <div class="added_price">125 <img src="img/manat.svg" alt=""></div>
                  <button class="delete"><img src="img/delete.svg" alt=""></button>
                </div>
              </div>
              <div class="added_project">
                <div class="added_img_box"><img src="img/added_img1.svg" alt=""></div>
                <div class="right_added_content">
                  <p class="title">Divar propkası</p>
                  <div class="added_price">125 <img src="img/manat.svg" alt=""></div>
                  <button class="delete"><img src="img/delete.svg" alt=""></button>
                </div>
              </div>
            </div>
            <div class="go_basket_box">
              <a href="basket.php" class="go_basket">Səbətə bax</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <?php
        include("includes/footer.php");
    ?>
</section>



<?php
    include("includes/script.php");
?>
