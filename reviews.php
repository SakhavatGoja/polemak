<?php
    include("includes/head.php");
?>


<section class="reviews">
    <?php
        include("includes/header.php");
    ?>
    <div class="reviews_container position-relative">
      <div class="container breadcrumps_container">
        <div class="row">
          <div class="breadcrumps">
            <a href="index.php" class="old_page">Məhsullar</a>
            <img src="img/breadcrump.svg" alt="">
            <span class="current_page">Papuc - Pəncərə üçün</span>
          </div>
        </div>
      </div>
      <div class="container">
          <div class="row">
            <div class="rewiews_title">
              <h1 class="title_head">Müştərilərimizin rəyləri.</h1>
              <p class="title_body">Keyfiyyətin kəmiyyətdən üstünlüyü.</p>
            </div>
          </div>
      </div>
      <div class="review_single w-100">
        <div class="container">
          <div class="row">
            <div class="rev_img" data-aos="fade-in" data-aos-duration="700" data-aos-easing="ease-in-out"><img src="img/review1.jpg" alt=""></div>
          </div>
        </div>
        <div class="stars_box" data-aos="fade-left" data-aos-duration="800" data-aos-easing="ease-in-out">
          <p class="company_name">Company name</p>
          <p class="company_info">Production plant is an industrial site, often a complex consisting of several buildings filled with machinery.</p>
          <div class="stars review5"></div>
        </div>
      </div>
      <div class="review_single w-100">
        <div class="container">
          <div class="row">
            <div class="rev_img" data-aos="fade-in" data-aos-duration="700" data-aos-easing="ease-in-out"><img src="img/review1.jpg" alt=""></div>
          </div>
        </div>
        <div class="stars_box" data-aos="fade-left" data-aos-duration="800" data-aos-easing="ease-in-out">
          <p class="company_name">Company name</p>
          <p class="company_info">Production plant is an industrial site, often a complex consisting of several buildings filled with machinery.</p>
          <div class="stars review4"></div>
        </div>
      </div>
      <div class="review_single w-100">
        <div class="container">
          <div class="row">
            <div class="rev_img" data-aos="fade-in" data-aos-duration="700" data-aos-easing="ease-in-out"><img src="img/review1.jpg" alt=""></div>
          </div>
        </div>
        <div class="stars_box" data-aos="fade-left" data-aos-duration="800" data-aos-easing="ease-in-out">
          <p class="company_name">Company name</p>
          <p class="company_info">Production plant is an industrial site, often a complex consisting of several buildings filled with machinery.</p>
          <div class="stars review3"></div>
        </div>
      </div>
      <ul class="pagination_project">
        <li><a href="#" class="active">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
      </ul>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>


<?php
    include("includes/script.php");
?>