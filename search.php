<?php
    include("includes/head.php");
?>


<section class="search">
    <?php
        include("includes/header.php");
    ?>
    <div class="search_landing">
        <div id="animatedBackground"></div>
        <div class="container">
            <div class="row">
                <div class="search_landing_inner w-100">
                    <p>AXTARIŞIN NƏTİCƏSİ</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="projects_container w-100">
          <p class="result_search">Axtarışın nəticəsi:  “ Propka “ üçün  <span>30</span> nəticə tapıldı!</p>
          <div class="projects_box" id="projects_box" data-columns>
            <div class="project grid-item">
              <div class="project_main">
                <div>
                  <img src="img/project.png" alt="">
                </div>
                <button>
                  <p>Divar propkası</p>
                  <img src="img/arrow.svg" alt="">
                </button>
              </div>
              <div class="project_bottom">
                  <div class="project_first_box">
                    <div class="projects_price">
                      <p>Divar propkası</p>
                      <span>50<img src="img/manat.svg" alt=""></span>
                    </div>
                    <div class="price_tabs">
                      <button data-id='1'>Haqqında<img src="img/arrow.svg" alt=""></button>
                      <button data-id='2'>Rəng və ölçü<img src="img/arrow.svg" alt=""></button>
                    </div>
                  </div>
                  <div class="project_tab_box project_about" data-tab="1">
                    <p>
                        Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building.
                         Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building. 
                    </p>
                  </div>
                  <div class="project_tab_box project_color" data-tab="2">
                    <div class="project_container_box">
                      <div class="pro_color_box">
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background:  #D0DAE8"></a>
                        <a href="#" style="background:  #D0DAE8"></a>
                      </div>
                      <div class="pro_size_box">
                        <div>8 mm</div>
                        <div>5 mm</div>
                        <div>5 mm</div>
                        <div>10 mm</div>
                      </div>
                    </div>
                  </div>
                  <div class="project_buttons_box">
                    <button type="button" data-toggle="modal" data-target="#addModal"> + səbətə əlavə et</button>
                    <button >birbaşa al</button>
                  </div>
                  <button class="quit"><img src="img/quit.svg" alt=""></button>
              </div>
            </div>
            <div class="project grid-item">
              <div class="project_main">
                <div>
                  <img src="img/project.png" alt="">
                </div>
                <button>
                  <p>Divar propkası</p>
                  <img src="img/arrow.svg" alt="">
                </button>
              </div>
              <div class="project_bottom">
                  <div class="project_first_box">
                    <div class="projects_price">
                      <p>Divar propkası</p>
                      <span>50<img src="img/manat.svg" alt=""></span>
                    </div>
                    <div class="price_tabs">
                      <p class="standart">Standart</p>
                      <button data-id='2'>Rəng və ölçü<img src="img/arrow.svg" alt=""></button>
                    </div>
                  </div>
                  <div class="project_tab_box project_about" data-tab="2">
                    <p>
                        Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building.
                         Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building. 
                    </p>
                  </div>
                  <div class="project_buttons_box">
                    <button type="button" data-toggle="modal" data-target="#addModal"> + səbətə əlavə et</button>
                    <button >birbaşa al</button>
                  </div>
                  <button class="quit"><img src="img/quit.svg" alt=""></button>
              </div>
            </div>
            <div class="project grid-item">
              <div class="project_main">
                <div>
                  <img src="img/project.png" alt="">
                </div>
                <button>
                  <p>Divar propkası</p>
                  <img src="img/arrow.svg" alt="">
                </button>
              </div>
              <div class="project_bottom">
                  <div class="project_first_box">
                    <div class="projects_price">
                      <p>Divar propkası</p>
                      <span>50<img src="img/manat.svg" alt=""></span>
                    </div>
                    <div class="price_tabs">
                      <button data-id='1'>Haqqında<img src="img/arrow.svg" alt=""></button>
                      <button data-id='2'>Rəng və ölçü<img src="img/arrow.svg" alt=""></button>
                    </div>
                  </div>
                  <div class="project_tab_box project_about" data-tab="1">
                    <p>
                        Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building.
                         Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building. 
                    </p>
                  </div>
                  <div class="project_tab_box project_color" data-tab="2">
                    <div class="project_container_box">
                      <div class="pro_color_box">
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background:  #D0DAE8"></a>
                        <a href="#" style="background:  #D0DAE8"></a>
                      </div>
                      <div class="pro_size_box">
                        <div>8 mm</div>
                        <div>5 mm</div>
                        <div>5 mm</div>
                        <div>10 mm</div>
                      </div>
                    </div>
                  </div>
                  <div class="project_buttons_box">
                    <button type="button" data-toggle="modal" data-target="#addModal"> + səbətə əlavə et</button>
                    <button >birbaşa al</button>
                  </div>
                  <button class="quit"><img src="img/quit.svg" alt=""></button>
              </div>
            </div>
            <div class="project grid-item">
              <div class="project_main">
                <div>
                  <img src="img/project.png" alt="">
                </div>
                <button>
                  <p>Divar propkası</p>
                  <img src="img/arrow.svg" alt="">
                </button>
              </div>
              <div class="project_bottom">
                  <div class="project_first_box">
                    <div class="projects_price">
                      <p>Divar propkası</p>
                      <span>50<img src="img/manat.svg" alt=""></span>
                    </div>
                    <div class="price_tabs">
                      <button data-id='1'>Haqqında<img src="img/arrow.svg" alt=""></button>
                      <button data-id='2'>Rəng və ölçü<img src="img/arrow.svg" alt=""></button>
                    </div>
                  </div>
                  <div class="project_tab_box project_about" data-tab="1">
                    <p>
                        Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building.
                         Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building. 
                    </p>
                  </div>
                  <div class="project_tab_box project_color" data-tab="2">
                    <div class="project_container_box">
                      <div class="pro_color_box">
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background:  #D0DAE8"></a>
                        <a href="#" style="background:  #D0DAE8"></a>
                      </div>
                      <div class="pro_size_box">
                        <div>8 mm</div>
                        <div>5 mm</div>
                        <div>5 mm</div>
                        <div>10 mm</div>
                      </div>
                    </div>
                  </div>
                  <div class="project_buttons_box">
                    <button type="button" data-toggle="modal" data-target="#addModal"> + səbətə əlavə et</button>
                    <button >birbaşa al</button>
                  </div>
                  <button class="quit"><img src="img/quit.svg" alt=""></button>
              </div>
            </div>
            <div class="project grid-item">
              <div class="project_main">
                <div>
                  <img src="img/project.png" alt="">
                </div>
                <button>
                  <p>Divar propkası</p>
                  <img src="img/arrow.svg" alt="">
                </button>
              </div>
              <div class="project_bottom">
                  <div class="project_first_box">
                    <div class="projects_price">
                      <p>Divar propkası</p>
                      <span>50<img src="img/manat.svg" alt=""></span>
                    </div>
                    <div class="price_tabs">
                      <button data-id='1'>Haqqında<img src="img/arrow.svg" alt=""></button>
                      <button data-id='2'>Rəng və ölçü<img src="img/arrow.svg" alt=""></button>
                    </div>
                  </div>
                  <div class="project_tab_box project_about" data-tab="1">
                    <p>
                        Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building.
                         Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building. 
                    </p>
                  </div>
                  <div class="project_tab_box project_color" data-tab="2">
                    <div class="project_container_box">
                      <div class="pro_color_box">
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background:  #D0DAE8"></a>
                        <a href="#" style="background:  #D0DAE8"></a>
                      </div>
                      <div class="pro_size_box">
                        <div>8 mm</div>
                        <div>5 mm</div>
                        <div>5 mm</div>
                        <div>10 mm</div>
                      </div>
                    </div>
                  </div>
                  <div class="project_buttons_box">
                    <button type="button" data-toggle="modal" data-target="#addModal"> + səbətə əlavə et</button>
                    <button >birbaşa al</button>
                  </div>
                  <button class="quit"><img src="img/quit.svg" alt=""></button>
              </div>
            </div>
            <div class="project grid-item">
              <div class="project_main">
                <div>
                  <img src="img/project.png" alt="">
                </div>
                <button>
                  <p>Divar propkası</p>
                  <img src="img/arrow.svg" alt="">
                </button>
              </div>
              <div class="project_bottom">
                  <div class="project_first_box">
                    <div class="projects_price">
                      <p>Divar propkası</p>
                      <span>50<img src="img/manat.svg" alt=""></span>
                    </div>
                    <div class="price_tabs">
                      <button data-id='1'>Haqqında<img src="img/arrow.svg" alt=""></button>
                      <button data-id='2'>Rəng və ölçü<img src="img/arrow.svg" alt=""></button>
                    </div>
                  </div>
                  <div class="project_tab_box project_about" data-tab="1">
                    <p>
                        Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building.
                         Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building. 
                    </p>
                  </div>
                  <div class="project_tab_box project_color" data-tab="2">
                    <div class="project_container_box">
                      <div class="pro_color_box">
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background:  #D0DAE8"></a>
                        <a href="#" style="background:  #D0DAE8"></a>
                      </div>
                      <div class="pro_size_box">
                        <div>8 mm</div>
                        <div>5 mm</div>
                        <div>5 mm</div>
                        <div>10 mm</div>
                      </div>
                    </div>
                  </div>
                  <div class="project_buttons_box">
                    <button type="button" data-toggle="modal" data-target="#addModal"> + səbətə əlavə et</button>
                    <button >birbaşa al</button>
                  </div>
                  <button class="quit"><img src="img/quit.svg" alt=""></button>
              </div>
            </div>
            <div class="project grid-item">
              <div class="project_main">
                <div>
                  <img src="img/project.png" alt="">
                </div>
                <button>
                  <p>Divar propkası</p>
                  <img src="img/arrow.svg" alt="">
                </button>
              </div>
              <div class="project_bottom">
                  <div class="project_first_box">
                    <div class="projects_price">
                      <p>Divar propkası</p>
                      <span>50<img src="img/manat.svg" alt=""></span>
                    </div>
                    <div class="price_tabs">
                      <button data-id='1'>Haqqında<img src="img/arrow.svg" alt=""></button>
                      <button data-id='2'>Rəng və ölçü<img src="img/arrow.svg" alt=""></button>
                    </div>
                  </div>
                  <div class="project_tab_box project_about" data-tab="1">
                    <p>
                        Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building.
                         Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building. 
                    </p>
                  </div>
                  <div class="project_tab_box project_color" data-tab="2">
                    <div class="project_container_box">
                      <div class="pro_color_box">
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background:  #D0DAE8"></a>
                        <a href="#" style="background:  #D0DAE8"></a>
                      </div>
                      <div class="pro_size_box">
                        <div>8 mm</div>
                        <div>5 mm</div>
                        <div>5 mm</div>
                        <div>10 mm</div>
                      </div>
                    </div>
                  </div>
                  <div class="project_buttons_box">
                    <button type="button" data-toggle="modal" data-target="#addModal"> + səbətə əlavə et</button>
                    <button >birbaşa al</button>
                  </div>
                  <button class="quit"><img src="img/quit.svg" alt=""></button>
              </div>
            </div>
            <div class="project grid-item">
              <div class="project_main">
                <div>
                  <img src="img/project.png" alt="">
                </div>
                <button>
                  <p>Divar propkası</p>
                  <img src="img/arrow.svg" alt="">
                </button>
              </div>
              <div class="project_bottom">
                  <div class="project_first_box">
                    <div class="projects_price">
                      <p>Divar propkası</p>
                      <span>50<img src="img/manat.svg" alt=""></span>
                    </div>
                    <div class="price_tabs">
                      <button data-id='1'>Haqqında<img src="img/arrow.svg" alt=""></button>
                      <button data-id='2'>Rəng və ölçü<img src="img/arrow.svg" alt=""></button>
                    </div>
                  </div>
                  <div class="project_tab_box project_about" data-tab="1">
                    <p>
                        Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building.
                         Factory, manufacturing plant or a production plant is an industrial site, often a complex consisting of several building. 
                    </p>
                  </div>
                  <div class="project_tab_box project_color" data-tab="2">
                    <div class="project_container_box">
                      <div class="pro_color_box">
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background: #D0DAE8"></a>
                        <a href="#" style="background:  #D0DAE8"></a>
                        <a href="#" style="background:  #D0DAE8"></a>
                      </div>
                      <div class="pro_size_box">
                        <div>8 mm</div>
                        <div>5 mm</div>
                        <div>5 mm</div>
                        <div>10 mm</div>
                      </div>
                    </div>
                  </div>
                  <div class="project_buttons_box">
                    <button type="button" data-toggle="modal" data-target="#addModal"> + səbətə əlavə et</button>
                    <button >birbaşa al</button>
                  </div>
                  <button class="quit"><img src="img/quit.svg" alt=""></button>
              </div>
            </div>

          </div>
          <ul class="pagination_project">
            <li><a href="#" class="active">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="modal fade modal_basket"  id="addModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="added_project_box">
            <button class="close esc" data-dismiss="modal" aria-hidden="true"><img src="img/esc.svg" alt=""></button>
            <p class="mobile_added_text">Səbətə əlave edildi!</p>
            <div class="add_center_box">
              <div class="added_project">
                <div class="added_img_box"><img src="img/added_img1.svg" alt=""></div>
                <div class="right_added_content">
                  <p class="title">Divar propkası</p>
                  <div class="added_price">125 <img src="img/manat.svg" alt=""></div>
                  <button class="delete"><img src="img/delete.svg" alt=""></button>
                </div>
              </div>
              <div class="added_project">
                <div class="added_img_box"><img src="img/added_img1.svg" alt=""></div>
                <div class="right_added_content">
                  <p class="title">Divar propkası</p>
                  <div class="added_price">125 <img src="img/manat.svg" alt=""></div>
                  <button class="delete"><img src="img/delete.svg" alt=""></button>
                </div>
              </div>
              <div class="added_project">
                <div class="added_img_box"><img src="img/added_img1.svg" alt=""></div>
                <div class="right_added_content">
                  <p class="title">Divar propkası</p>
                  <div class="added_price">125 <img src="img/manat.svg" alt=""></div>
                  <button class="delete"><img src="img/delete.svg" alt=""></button>
                </div>
              </div>
            </div>
            <div class="go_basket_box">
              <a href="basket.php" class="go_basket">Səbətə bax</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
        include("includes/footer.php");
    ?>
</section>


<?php
    include("includes/script.php");
?>